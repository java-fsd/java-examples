package com.classpath.date;

import java.time.DayOfWeek;
import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.concurrent.TimeUnit;

public class LocalDateDemo {
	
	public static void main(String[] args) {
		LocalDate date = LocalDate.now();
		LocalDateTime time = LocalDateTime.now();
		
		System.out.println("Current date: "+ date);
		System.out.println("Current time:: "+ time);
		
		LocalDate dob = LocalDate.of(1983, 5, 24);
		
		dob.getDayOfMonth();
		DayOfWeek dayOfWeek = dob.getDayOfWeek();
		System.out.println("Your birthday falls on "+dayOfWeek);
		String dobFormat = dob.format(DateTimeFormatter.ofPattern("dd-MM-yy"));
		System.out.println("Format of date is :: "+ dobFormat);
		
		/*
		 * 1. take your date of birth 
		 *    add 40 years from your date of birth
		 *    find out which day will it fall on your 40th birthday
		 *    
		 * 2. Find out the duration of days between 09-06-1947 till 09-06-2022
		 * 
		 * 3. If you have to apply for 13 days leave, from tomorrow (08-06-22), on what day of the week, will you rejoin
		 *    // you need to consider weekends not part of the leaves    
		 */
		
		LocalDate fromDate = LocalDate.of(1947, 6, 9);
		LocalDate toDate = LocalDate.of(2022,  6, 9);
		
		long days = Duration.between(fromDate.atStartOfDay(), toDate.atStartOfDay()).toDays();
		System.out.println("Total number of days :: "+ days);
		
		
		LocalDate currentDate = LocalDate.of(2022, 6, 8);
		LocalDate joiningDate = currentDate.plusDays(13);
		DayOfWeek joiningDayOfTheWeek = joiningDate.getDayOfWeek();
		System.out.println("Joining day of the week :: "+ joiningDayOfTheWeek);
		
	}

}
