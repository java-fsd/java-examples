package com.classpath.functional;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;

public class PredicateDemo {
	
	public static void main(String[] args) {
		//functional descriptor for a predicate is: takes input return boolean
		Predicate<Integer> greaterThan18 = value -> value > 18;
		boolean result = greaterThan18.test(15);
		//System.out.println("Result :: "+ result);
		
		User ramesh = new User(12, "Ramesh");

		User suresh = new User(14, "Suresh");
		
		User vinay= new User(15, "Vinay");
		
		User hari = new User(24, "Hari");
		
		Predicate<User> userAgeGtThan18 = user -> user.getAge() > 18;
		Predicate<User> userAgeLtThan18 = user -> user.getAge() < 18;
		
		boolean rameshIsGtThan18 = userAgeGtThan18.test(ramesh);
		boolean sureshIsGtThan18 = userAgeGtThan18.test(suresh);
		boolean vinayIsGtThan18 = userAgeGtThan18.test(vinay);
		boolean hariIsGtThan18 = userAgeGtThan18.test(hari);
		
		
		System.out.println(" rameshIsGtThan18 :: "+ rameshIsGtThan18);
		System.out.println(" sureshIsGtThan18 :: "+ sureshIsGtThan18);
		System.out.println(" vinayIsGtThan18 :: "+ vinayIsGtThan18);
		System.out.println(" hariIsGtThan18 :: "+ hariIsGtThan18);
		
		
		
		boolean rameshIsLtThan18 = userAgeLtThan18.test(ramesh);
		boolean sureshIsLtThan18 = userAgeLtThan18.test(suresh);
		boolean vinayIsLtThan18 = userAgeLtThan18.test(vinay);
		boolean hariIsLtThan18 = userAgeLtThan18.test(hari);
		
		System.out.println("***********************************");
		
		System.out.println(" rameshIsLtThan18 :: "+ rameshIsLtThan18);
		System.out.println(" sureshIsLtThan18 :: "+ sureshIsLtThan18);
		System.out.println(" vinayIsLtThan18 :: "+ vinayIsLtThan18);
		System.out.println(" hariIsLtThan18 :: "+ hariIsLtThan18);
		
		//functional descriptor for a predicate is: takes input return void
		Consumer<User> printUser = user-> System.out.println(user);
		
		//functional descriptor for a predicate is: takes input return output
		Function<User, String> userToNameFn = (user) -> user.getName();

		Function<User, Integer> userToAgeFn = (user) -> user.getAge();
		
		
		List<User> users = new ArrayList<>();
		
		users.add(ramesh);
		users.add(suresh);
		users.add(hari);
		users.add(vinay);
		
		System.out.println("***********************************");
		
		users
			.stream()
			.filter(userAgeLtThan18)
			.map(userToNameFn)
			.forEach((userGt18) -> System.out.println(userGt18));
	}
}

class User {
	private int age;
	private String name;
	
	public User(int age, String name) {
		super();
		this.age = age;
		this.name = name;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String toString() {
		return "User [age=" + age + ", name=" + name + "]";
	}
	
	
}





