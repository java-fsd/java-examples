package com.classpath.interfacedemo.client;

import java.util.Scanner;
import com.classpath.interfacedemo.GooglePay;
import com.classpath.interfacedemo.PhonePay;
//fully qualified class name
import com.classpath.interfacedemo.PhoneRecharge;

public class PhoneRechargeClient {

	public static void main(String[] args) {
		PhoneRecharge phoneRecharge = null;
		
		Scanner scanner  = new Scanner(System.in);
		System.out.println("Please enter your option ");

		System.out.println("1: Google Pay ");
		System.out.println("2: Phone Pay");
		
		int option = scanner.nextInt();
		
		System.out.println("Please enter the phone number to recharge");
		
		String phoneNumber = scanner.next();

		System.out.println("Please enter the amount to recharge");
		
		double amount = scanner.nextDouble();
		
		switch(option) {
		case 1: 
			phoneRecharge = new GooglePay();
			break;
		case 2:
			phoneRecharge = new PhonePay();
			break;
		default:
			phoneRecharge = new GooglePay();
		}
		
		phoneRecharge.recharge(phoneNumber , amount);
		
		scanner.close();
		
	}

}
