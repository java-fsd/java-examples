package com.classpath.interfacedemo;


public class AmazonPay implements Payment {

	@Override
	public boolean acceptPayment(String merchant, String customer, double amount, String notes) {
		System.out.println("Making a payment of Rs "+ amount + " from "+ merchant+ " to "+ customer + " notes: "+notes + " using Amazon Pay");
		System.out.println(" Cash back :: "+ (5 * amount));
		return true;
	}

}
