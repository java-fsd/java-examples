package com.classpath.collections;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

public class HashMapDemo {
	
	public static void main(String[] args) {
		//key should be unique, value can be duplicate
		//key should override hashcode and equals method
		
		Map<String, User> users = new HashMap<>();
		
		users.put("Rama", new User(1, "Rama", 26));
		
		users.put("Hari", new User(2, "Hari", 22));
		users.put("Vinay", new User(3, "Vinay", 22));
		users.put("Vinod", new User(4, "Vinod", 22));
		users.put("mary", new User(5, "Mary", 22));
		users.put("Mithun", new User(6, "Mithun", 22));
		users.put("Sakshi", new User(7, "Sakshi", 22));
		
		
		/*
		 * boolean isRamaPresent = users.containsKey("Rama");
		 * 
		 * User hari = users.get("Hari");
		 * 
		 * Set<String> userNames = users.keySet();
		 * 
		 * users.remove("Mithun");
		 */
		
		//iterator over a map
		
		var entrySet = users.entrySet();
		var mapEntryIt = entrySet.iterator();
		
		while(mapEntryIt.hasNext()) {
			var entry = mapEntryIt.next();
			String key = entry.getKey();
			User value = entry.getValue();
			
			System.out.println("Username :: "+ key);
			System.out.println("User :: "+ value);
		}
	}
}
