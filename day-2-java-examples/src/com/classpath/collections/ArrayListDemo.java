package com.classpath.collections;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

public class ArrayListDemo {
	
	public static void main(String[] args) {
		
		List<String> namesOfEmployees = new ArrayList<>();
		
		String[] names = new String[10];
		
		namesOfEmployees.add("Rama");
		namesOfEmployees.add("Hari");
		namesOfEmployees.add("Vinay");
		namesOfEmployees.add("Vinod");
		namesOfEmployees.add("mary");
		namesOfEmployees.add("Mithun");
		namesOfEmployees.add("Sakshi");
		
		boolean isMaryPresent = namesOfEmployees.contains("mary");
		String nameAtIndex0 = namesOfEmployees.get(0);
		
		System.out.println("Is Mary present ? "+ isMaryPresent);
		System.out.println("Name at index 0 "+ nameAtIndex0);
		
		//iterate over any array
		
		/*
		 * for(int index = 0; index < namesOfEmployees.size(); index ++) {
		 * System.out.println(namesOfEmployees.get(index)); }
		 */
		System.out.println(" ***************************************");
		//iterate over an array using foreach
		/*
		 * for(String name: namesOfEmployees) { System.out.println(name); }
		 */
		
		//third way to iterate using iterator
		
		/*
		 * Iterator<String> it = namesOfEmployees.iterator();
		 * 
		 * while(it.hasNext()) { String name = it.next();
		 * System.out.println("Name is "+name); }
		 */
		ListIterator<String> lit = namesOfEmployees.listIterator();
		
		while(lit.hasNext()) {
			System.out.println("Name is "+lit.next());
		}
		
		System.out.println(" -------------------------------------------");
		while(lit.hasPrevious()) {
			System.out.println("Name is "+lit.previous());
		}
		
	}

}
