package com.classpath.packagedemo.packb;

import com.classpath.packagedemo.packa.ExportedClass;
import com.classpath.packagedemo.packa.Server;
import com.classpath.packagedemo.packa.User;

public class Client{
	
	public static void main(String[] args) {
		/*
		 * Server server = new Server(); Client client = new Client();
		 * //server.protectedMethod(); //client.protectedMethod();
		 * 
		 * server.publicMethod(); ExportedClass obj = new ExportedClass();
		 * obj.exportedMethod(); //client.publicMethod();;
		 * 
		 * User ramesh = new User(12,"Ramesh", 32);
		 * 
		 * System.out.println("Age :: "+ramesh.getAge());
		 * System.out.println("Name :: "+ramesh.getName());
		 * System.out.println("Id :: "+ramesh.getId());
		 * 
		 * System.out.println("Ramesh :: "+ ramesh);
		 */
		User ramesh1 = new User(12,"Ramesh", 32);
		
		User ramesh2 = new User(12,"Ramesh", 32);
		
		User ramesh3 = ramesh1;
		
		System.out.println("Are ramesh1 and ramesh2 are same ? "+ (ramesh1 == ramesh2));
		System.out.println("Are ramesh1 and ramesh2 are same ? "+ ramesh1.equals(ramesh2));
		
		System.out.println("Are ramesh3 and ramesh1 are same ? "+ (ramesh3 == ramesh1));
	}

}
