package com.classpath.packagedemo.packa;

// a  class can be declared as public or default
 public class Server {
	 
	 private void privateMethod() {
		 System.out.println("Private method ::");
	 }
	 
	 
	 void defaultMethod() {
		 System.out.println("Default method :: ");
	 }
	 
	 protected void protectedMethod() {
		 System.out.println("Protected method");
	 }
	 
	 public void publicMethod() {
		 System.out.println("public method ::");
	 }

}
