package com.classpath.emp.dao;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Optional;
import java.util.Set;

import com.classpath.emp.model.Employee;

//TODO - 6
/*
 * For a given employee if, I should be able to fetch all the dependents
 * (Map, Key - empId, value - > set of dependents)
 */
public class EmployeeRepository {
	
	private Set<Employee> employees;
	
	public EmployeeRepository() {
		
		this.employees = new HashSet<>();
	}
	
	//TODO-2
	/*
	 * Constructor to initialize employees to a HashSet
	 * method imlementation for inserting, finding, getting, deleting employees
	 */
	
	public Employee addEmployee(Employee employee) {
		this.employees.add(employee);
		return employee;
	}
	
	//get, delete, findbyId, update
	
	public Set<Employee> fetchAllEmployees(){
		return this.employees;
	}
	
	
	//refactor this with lamdba expression/ stream operation
	public Optional<Employee> findEmployeeById(int empId) {
		return this.employees.stream()
				.filter(employee -> employee.getEmpId() == empId)
				.findAny();
	}
	
	//stream operations
	
	public void deleteEmployeeById(int empId) {
		this.employees.removeIf(employee -> employee.getEmpId() == empId);
	}

}
