package com.classpath.emp.client;

import java.util.Scanner;

import com.classpath.emp.dao.EmployeeRepository;
import com.classpath.emp.model.Employee;
import com.classpath.emp.service.EmployeeService;

//TODO - 9
//one more option to find dependents by passing the employee id.
public class EmployeeClient {
	
	public static void main(String[] args) {
		//TODO-4
		/*
		 * Scanner class
		 * ask for option
		 * 1 -> insert
		 * 2-> findll
		 * 3 -> find by employeeId 
		 *    ->If the empId is not present, then throw exception called IllegalArgumentException
		 * 4 -> update
		 * 5 -> delete
		 */
		Scanner scanner = new Scanner(System.in);
		EmployeeRepository employeeRepository = new EmployeeRepository();
		EmployeeService employeeService = new EmployeeService(employeeRepository);
		boolean flag = true;
		while(flag) {
			
			System.out.println();
			System.out.println();
			System.out.println("Enter your option ::");
			System.out.println(" 1 -> insert employee");
			System.out.println(" 2 -> Find employee by id");
			System.out.println(" 3 -> delete employee");
			System.out.println(" 5 -> Exit");
			
			int option = scanner.nextInt();
			
			switch(option) {
				case 1:
					System.out.println("Enter the employee name");
					String name = scanner.next();
					System.out.println("Enter the employee salary");
					double salary = scanner.nextDouble();
					Employee employee = new Employee(name, salary);
					Employee savedEmployee = employeeService.saveEmployee(employee);
					System.out.println("Saved employee from the db:: "+ savedEmployee );
					break;
				case 2:
					System.out.println("Enter the employee id");
					int empId = scanner.nextInt();
					try {
						Employee fetchedEmployee = employeeService.findEmployeeById(empId);
						System.out.println("Employee Fetched is "+ fetchedEmployee);
					} catch(IllegalArgumentException exception) {
						System.out.println(" Invalid employee id passed :: "+ empId);
					}
					break;
					
				case 3:
					System.out.println("Enter the employee id");
					int id = scanner.nextInt();
					employeeService.deleteEmployeeById(id);
					break;
				case 5:
					flag = false;
					break;
			}
		}
		System.out.println("Thanks for using this application::");
		scanner.close();
	}

}
