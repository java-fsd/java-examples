
public class PermanentEmployee extends Employee{
	
	private long leaves = 30;
	private double salary = 15000d;
	
	public PermanentEmployee(String name) {
		super(name);
	}

	

	@Override
	public void applyForLeave(int noOfDays) {
			if(this.leaves - noOfDays >= 0 ) {
				this.leaves = this.leaves - noOfDays;
			}
	}

	@Override
	public double paySalary() {
		return this.salary;
	}
	
	public long getAvailableLeaves() {
		return this.leaves;
	}

}
