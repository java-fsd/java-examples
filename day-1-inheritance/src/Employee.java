
public abstract class Employee {
	
	private long id;
	private String name;
	private static long counter;
	
	public Employee(String name) {
		this.id = ++counter;
		this.name = name;
	}
	
	public long getEmpId() {
		return this.id;
	}
	
	public abstract void applyForLeave(int noOfDays);
	
	public abstract double  paySalary();
	

}
