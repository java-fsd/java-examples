
//class
public class SavingsAccount {
    //encapsulation
    //instance variables
    //all the instance variable are assigned with default values
     /*
      * int - 0
      * boolean - false
      * char - ''
      * float, double - 0.0
      * object - null
      */
      //The scope of the instance variable is tied to the instance
    private long accountId;
    private String name;
    private double accountBalance;
    //behavior

    //Constructor
    /*
     * do not have a return type
     * Used to initialize the instance variables
     * Should be the same as the class name
     * By default if no constructor is explicitly created, Compiler will create a default no argument constructor
     * If we provide a constructor, then Compiler will not create a default no argument constructor
     */
    public SavingsAccount(String name, double accountBalance){
        this.name = name;
        this.accountBalance = accountBalance;
    }

    //method definition -> 
    //[private/protected/default/public] [static/final/syncrhonized] [return type/ void] [method-name] [arguments]
    public void deposit(double amount){
        //if you declare a variable inside the method it is referred as method local variable
        /*
         * Method local variables have to initialized explicitly - No default values will be assigned
         * 
         */
        //The scope of method local variables is only limited to the method block.
        this.accountBalance = this.accountBalance + amount;
    }

    public double withdraw(double amount){
        //if the user has sufficient account balance -> deduct the amount
        // if the user does not have sufficent balance -> deny the request

        if(this.accountBalance - amount >= 0) {
            this.accountBalance  = this.accountBalance - amount;
            return amount;
        }
        return 0;
    }

    public double getCurrentBalance(){
        return this.accountBalance;
    }

}
