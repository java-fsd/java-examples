
public class Loop {

    public static void main(String[] args) {
        
        //for loop
        int[] ages = new int[10];

        //assinging the values
        ages[0] = 12;
        ages[1] = 22;
        ages[2] = 34;
        ages[3] = 45;
        ages[4] = 17;
        ages[5] = 26;
        ages[6] = 14;
        ages[7] = 36;
        ages[8] = 15;
        ages[9] = 23;

        for (int age = 0 ; age < ages.length; age++) {
            int value = ages[age];    
            if( value < 5){
                System.out.println("Infant "+ value);    
            } else if (value >=5 && value < 18){
                System.out.println("Minor "+ value);    
            }  else if (value >=18 && value < 58){
                System.out.println("Adult "+ value);    
            } else {
                System.out.println("Senior "+ value);
            }
        }
    }
   
}
