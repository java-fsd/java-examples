package com.classpath.concurrency.producerconsumer;

import java.util.LinkedList;
import java.util.Queue;

public class Inventory {

	private final Queue<Message> inventory = new LinkedList<>();
	private final int capacity;
	private final Object FULL_QUEUE = new Object();
	private final Object EMPTY_QUEUE = new Object();

	public Inventory(int capacity) {
		this.capacity = capacity;
	}

	public boolean isFull() {
		return this.inventory.size() == this.capacity;
	}

	public boolean isEmpty() {
		return this.inventory.isEmpty();
	}

	public void waitOnFull() {
		synchronized (FULL_QUEUE) {
			try {
				FULL_QUEUE.wait();
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	public void waitOnEmpty() {
		synchronized (EMPTY_QUEUE) {
			try {
				EMPTY_QUEUE.wait();
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	public void produce(Message mesage) {
		synchronized (inventory) {
			this.inventory.add(mesage);
		}
	}

	public Message consume() {
		synchronized (inventory) {
			return this.inventory.poll();
		}
	}

	public void notifyOnEmpty() {
		synchronized (EMPTY_QUEUE) {
			EMPTY_QUEUE.notifyAll();
		}

	}

	public void notifyOnFull() {
		synchronized (FULL_QUEUE) {
			FULL_QUEUE.notifyAll();
		}
	}
}
