package com.classpath.concurrency.producerconsumer;

import java.time.Duration;
import java.time.temporal.ChronoUnit;
import java.util.Random;
import java.util.UUID;

public class Consumer implements Runnable {
private final Inventory inventory;
	
	public Consumer (Inventory inventory) {
		this.inventory = inventory;
	}

	@Override
	public void run() {
		consume();
	}

	private void consume() {
		//generate the message until the queue is full
		
		//this should run indefinitly
		while(true) {
			while(this.inventory.isEmpty()) {
				System.out.println("Inventory is empty ::  Consumer is waiting");
				this.inventory.waitOnEmpty();
			}
			Message message = this.inventory.consume();
			System.out.println("Message is consumed :: "+ message);
		
			this.inventory.notifyOnFull();
		}
	}

}
