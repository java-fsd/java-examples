package com.classpath.concurrency.producerconsumer;

import java.time.Duration;
import java.time.temporal.ChronoUnit;
import java.util.Random;
import java.util.UUID;

public class Producer implements Runnable {
	
	private final Inventory inventory;
	
	public Producer (Inventory inventory) {
		this.inventory = inventory;
	}

	@Override
	public void run() {
		produce();
	}

	private void produce() {
		//generate the message until the queue is full
		
		//this should run indefinitly
		while(true) {
			
			Message message = new Message(new Random().nextInt(), UUID.randomUUID().toString());
			
			while(this.inventory.isFull()) {
				System.out.println("Inventory is full ::  Producer is waiting");
				this.inventory.waitOnFull();
			}
			try {
				Thread.sleep(Duration.of(1, ChronoUnit.SECONDS).toMillis());
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			System.out.println("Producer is producing the message:: "+ message);
			this.inventory.produce(message);
			this.inventory.notifyOnEmpty();
		}
	}

}
