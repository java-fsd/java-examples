package com.classpath.concurrency;

public class PrinterDemo {

	public static void main(String[] args) throws InterruptedException {

		Printer printer = new Printer(5);
		

		Thread t1 = new Thread(new PrinterTask(printer), "Ravi");
		Thread t2 = new Thread(new PrinterTask(printer), "Kishore");
		Thread t3 = new Thread(new PrinterTask(printer), "Hari");
		Thread t4 = new Thread(new PrinterTask(printer), "Vinay");

		t1.start();
		t2.start();
		t3.start();
		t4.start();

		t1.join();
		t2.join();
		t3.join();
		t4.join();

		System.out.println("Completed all the printer tasks");
	}

}
